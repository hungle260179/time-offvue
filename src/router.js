import {createRouter, createWebHistory} from "vue-router";
import Register from './views/Register.vue';
import Login from './views/Login.vue';
import CreateForm from '../src/views/pages/Form/new.vue';
import DetailForm from '../src/views/pages/Form/detail.vue';
import Dashboard from './layouts/Dashboard.vue'
import ManageUser from "../src/views/pages/Form/manage.vue";
import InfoAccount from "../src/views/pages/info.vue";
const isAuthenticated =  window.localStorage.getItem('token');
const routes = [

  //public router
    {
        path: '/register',
        name: 'register',
        component: !isAuthenticated  ?Register :  Dashboard
    },
    {
        path: '/login',
        name: 'login',
        component: !isAuthenticated  ?Login :  Dashboard
    },


    //private router
    {
        path:'/',
        name:'dashboard',
        component: isAuthenticated  ? Dashboard : Login

    },
    {
        path:'/form',
        name:'form',
        component: isAuthenticated  ? CreateForm : Login,
    },
    {
        path:'/detail',
        name:'detail',
         component: isAuthenticated  ? DetailForm : Login,
    },
    {
        path: '/manage',
        name: 'manage-user',
        component: isAuthenticated  ? ManageUser : Login,
        
    },
    {
        path: '/info',
        name: 'account-info',
        component: isAuthenticated  ?  InfoAccount : Login,
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
